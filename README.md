# VTK Show 2016
Dit is de website die de geprojecteerd zal worden als achtergrond op de VTK Show.   
Alles is met javascipt geanimeerd met voorgemaakte mockup foto's.

First install install all necessary packages specified by package.json by executing: `npm install`   
Then you can start the app: `npm start`   
And visit in a browser at `localhost:3000`

Core functionality can be found in public/js/show.js
